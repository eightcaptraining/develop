﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AWSEightCap.Core
{
    public class CustomException : Exception
    {
        public CustomException(String message) :
           base("[InternalServerError] " + message)
        {
        }
    }

    public class InternalServerErrorException : Exception
    {
        public InternalServerErrorException(String message) :
           base("[InternalServerError] " + message)
        {
        }
    }

    public class ForbiddenError : Exception
    {
        public ForbiddenError(String message) :
           base("[Forbidden] " + message)
        {
        }
    }

    public class PropertyIsRequireException : Exception
    {
        public PropertyIsRequireException(String message) :
           base("[BadRequest] " + message)
        {
        }
    }

    public class ValidationException : Exception
    {
        public ValidationException(String message) :
           base("[BadRequest] " + message)
        {
        }
    }
   
    public class ConflictException : Exception
    {
        public ConflictException(String message) :
           base("[BadRequest] " + message)
        {
        }
    }

    public class NotFoundException : Exception
    {
        public NotFoundException(String message) :
           base("[NotFound] " + message)
        {
        }
    }
    public class AddPersonException : Exception
    {
        public AddPersonException(String message) :
           base("[InternalServerError] " + message)
        {
        }
    }
}
