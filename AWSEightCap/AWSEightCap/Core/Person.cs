﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace AWSEightCap.Core
{
    [Table("8c_person")]
    public class Person
    {
        public Person()
        {
            this.Phones = new HashSet<Phone>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("person_id")]
        public int Id { get; set; }
        [Column("person_name")]
        public string Name { get; set; }
        [Column("person_dob")]
        public DateTime? DoB { get; set; }
        [Column("person_email")]
        public string Email { get; set; }

        public virtual ICollection<Phone> Phones { get; set; }
    }
}
