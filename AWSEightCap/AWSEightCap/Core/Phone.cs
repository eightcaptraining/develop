﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace AWSEightCap.Core
{
    [Table("8c_phones")]
    public class Phone
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("phone_id")]

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [Column("fk_person_id")]
        [JsonProperty(PropertyName = "person_id")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Person Id is required.")]
        public int PersonId { get; set; }
        [Column("phone_number")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Phone is required.")]
        [JsonProperty(PropertyName = "phone")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone format is not valid.")]
        public string PhoneNumber { get; set; }
        [JsonIgnore]
        public virtual Person Person { get; set; }
    }
}
