﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AWSEightCap.Request
{
    public class PersonRequest
    {        
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "dob")]
        public string DoB { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
    }
}
