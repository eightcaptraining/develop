﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AWSEightCap.Request
{
    public class SearchPersonRequest
    {        
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "start")]
        public int Start { get; set; }
        [JsonProperty(PropertyName = "limit")]
        public int Limit { get; set; }
    }
}
