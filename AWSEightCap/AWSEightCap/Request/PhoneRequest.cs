﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AWSEightCap.Request
{
    public  class PhoneRequest
    {
         
       
        [JsonProperty(PropertyName = "person_id")] 
        public string PersonId { get; set; } 
        [JsonProperty(PropertyName = "phone")] 
        public string PhoneNumber { get; set; }
        [JsonProperty(PropertyName = "error")]
        public string Error { get; set; }
    }
}
