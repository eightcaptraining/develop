using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Amazon.Lambda.Core;
using AWSEightCap.Core;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using Microsoft.EntityFrameworkCore;
using AWSEightCap.Request;
using AWSEightCap.Response;
using Amazon.Lambda.APIGatewayEvents;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AWSEightCap
{
    public class Function
    {
        /// <summary>
        /// Insert a person

        public PersonResponse AddPersonFunctionHandler(PersonRequest input, ILambdaContext context)
        {
            if (string.IsNullOrWhiteSpace(input.Name))
            {
                throw new InternalServerErrorException("Request body format is wrong");
            }

            //Check email address is valid
            if (!string.IsNullOrWhiteSpace(input.Email))
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(input.Email.ToString());
                if (!match.Success)
                {
                    throw new ValidationException("Validation error: Email address is invalid");
                }
            }

            //Check date of birth is valid
            DateTime dataOfBirth = new DateTime();
            if (!string.IsNullOrWhiteSpace(input.DoB))
            {
                string[] formats = { "d/MM/yyyy", "dd/MM/yyyy" };
                
                var isValidFormat = DateTime.TryParseExact(input.DoB, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dataOfBirth);

                if (!isValidFormat)
                {
                    throw new ValidationException("Validation error: Date of birth is invalid");
                }
                else if (dataOfBirth.Date >= DateTime.Now.Date)
                {
                    throw new ValidationException("Validation error: Date of birth must be less than today");
                }
            } 
            

            //Begin add person to database
            var person = new Person();
            try
            {
                using (var myContext = new MySQLContext())
                {
                    person.Name = input.Name;
                    if (!string.IsNullOrWhiteSpace(input.DoB))
                        person.DoB = dataOfBirth;
                    person.Email = string.IsNullOrWhiteSpace(input.Email)? "": input.Email;
                    myContext.Person.Add(person);
                    myContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (ex.GetBaseException().GetType() == typeof(MySqlException))
                {
                    var exception = ((MySqlException)ex.InnerException);
                    Int32 ErrorCode = exception.Number;
                    switch (ErrorCode)
                    {
                        case 1062:  // Unique constraint error
                            throw new ConflictException("The name already exists");
                        default:
                            throw new AddPersonException("An unknown error has occurred. Please try again.");
                    }
                }
                else
                {
                    throw new CustomException(ex.Message);
                }
            }
            return new PersonResponse() { Id = person.Id, Name = person.Name, DoB = person.DoB == null ? "": person.DoB.Value.ToString("dd/MM/yyyy"), Email = person.Email, Phones = new List<string>() };
        }

        /// <summary>
        /// Get phone list of person

        public List<string> GetPhoneOfPersonFunctionHandler(int id, ILambdaContext context)
        {
            List<string> phones = new List<string>();
            using (var myContext = new MySQLContext())
            {
                var personId = id;
                var person = myContext.Person.Include(c => c.Phones).Where(x => x.Id == personId).SingleOrDefault();
                if (person == null)
                {
                    throw new NotFoundException("Not found the person with id " + id);
                }
                phones = person.Phones.Select(x => x.PhoneNumber).ToList();
            }

            return phones;
        }

        /// <summary>
        /// Get persons list

        public RootObject GetAllPersonFunctionHandler(SearchPersonRequest input, ILambdaContext context)
        {
            List<PersonResponse> persons = new List<PersonResponse>();
            try
            {
                using (var myContext = new MySQLContext())
                {
                    var items = myContext.Person.Include(c => c.Phones).Where(x => string.IsNullOrWhiteSpace(input.Name) || x.Name.Contains(input.Name)).Skip(input.Start > 0 ? input.Start : 0).Take(input.Limit > 0 ? input.Limit : 0).ToList();
                    foreach (var item in items)
                    {
                        PersonResponse person = new PersonResponse()
                        {
                            Id = item.Id,
                            Name = item.Name,
                            DoB = item.DoB != null? item.DoB.Value.ToString("dd/MM/yyyy"): "",
                            Email = item.Email
                        };
                        person.Phones = item.Phones.Select(x => x.PhoneNumber).ToList();
                        persons.Add(person);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new InternalServerErrorException(ex.Message);
            }
            Pagination paging = new Pagination()
            {
                next = persons.Count != 0 ? (input.Limit > persons.Count ? input.Start : input.Start + input.Limit) : 0,
                previous = persons.Count != 0 ? (input.Start > input.Limit ? input.Start - input.Limit : 0) : 0
            };
            RootObject payload = new RootObject()
            {
                data = persons,
                pagination = paging
            };
            return payload;
        }

        /// <summary>
        /// Add a phone to person

        public object AddPhoneForPerson(PhoneRequest requestPhone, ILambdaContext context)
        {
            if(requestPhone.Error.Length > 0)
            {
                throw new ValidationException(requestPhone.Error);
            }
            string error = "";
            if (string.IsNullOrWhiteSpace(requestPhone.PersonId.ToString())) error += "Person Id required!";
            if (!string.IsNullOrWhiteSpace(error))
            {
                throw new PropertyIsRequireException(error);
            }
            if (string.IsNullOrWhiteSpace(requestPhone.PhoneNumber))
            {
                error = "Phone Number is required!";
            }

            if (!string.IsNullOrWhiteSpace(error))
            {
                throw new PropertyIsRequireException(error);
            }
            var phonePerson = new Phone();
            phonePerson.PersonId = int.Parse(requestPhone.PersonId);
            phonePerson.PhoneNumber = requestPhone.PhoneNumber;
            if (phonePerson.PersonId > 0)
                {
                    string phone = phonePerson.PhoneNumber;

                    Regex phonePattern = new Regex(@"(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})");

                    Match match = phonePattern.Match(phone);
                    if (!match.Success)
                    {
                        throw new ValidationException("Phone is not right format!");
                    }
                    else
                    {
                        var myContext = new MySQLContext();
                        var person = myContext.Person.Where(t => t.Id == phonePerson.PersonId).FirstOrDefault();
                        if (person == null)
                        {
                             throw new NotFoundException("Person not found!");
                        }
                        else
                        {
                            var phoneSearch = myContext.Phone.Where(t => t.PersonId == phonePerson.PersonId && t.PhoneNumber.Equals(phonePerson.PhoneNumber)).FirstOrDefault();
                            if (phoneSearch == null)
                            {


                                var phonePersonData = new Phone();
                                phonePersonData.PersonId = phonePerson.PersonId;
                                phonePersonData.PhoneNumber = phonePerson.PhoneNumber;

                                myContext.Phone.Add(phonePersonData);
                                myContext.SaveChanges();
                                return phonePersonData;
                            }
                            else
                            {
                                throw new ValidationException("This Phone number is already exist!");
                            }
                    }
                }
            }
            else
            {
                throw new ValidationException("Person id is required");
            }

        }
    }
}
