﻿using Amazon.Lambda.APIGatewayEvents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace AWSEightCap.Response
{
    public class EightCapResponse
    {
        private static string ErrorBody(int status, string detail)
        {
            var data = new
            {
                errors = new
                {
                    status = status,
                    detail = detail
                }
            };

            var json = JsonConvert.SerializeObject(data);
            return json;
        }
        public static Dictionary<string, string> hederType = new Dictionary<string, string> { { "Content-Type", "application/json" } };
        public static APIGatewayProxyResponse Ok(string body)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.OK,
                Body = body,
                Headers = EightCapResponse.hederType
            };
        }
        public static APIGatewayProxyResponse BadRequest(string body)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.BadRequest,
                Body = EightCapResponse.ErrorBody((int)HttpStatusCode.BadRequest, body),
                Headers = EightCapResponse.hederType
            };
        }
        public static APIGatewayProxyResponse Error(Exception ex)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.InternalServerError,
                Body = EightCapResponse.ErrorBody((int)HttpStatusCode.InternalServerError, ex.Message),
                Headers = EightCapResponse.hederType
            };
        }
        public static APIGatewayProxyResponse NotFound(string body)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.NotFound,
                Body = EightCapResponse.ErrorBody((int)HttpStatusCode.NotFound, body),
                Headers = EightCapResponse.hederType
            };
        }
        public static APIGatewayProxyResponse Created(string body)
        {
            return new APIGatewayProxyResponse
            {
                StatusCode = (int)HttpStatusCode.Created,
                Body = body,
                Headers = EightCapResponse.hederType
            };
        }
    }
}
