﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AWSEightCap.Response
{
    public class PersonResponse
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "dob")]
        public string DoB { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "phones")]
        public List<string> Phones { get; set; }
    }

    public class Pagination
    {
        [JsonProperty(PropertyName = "next")]
        public int next { get; set; }
        [JsonProperty(PropertyName = "previous")]
        public int previous { get; set; }
    }

    public class RootObject
    {
        public List<PersonResponse> data { get; set; }
        public Pagination pagination { get; set; }
    }
}
